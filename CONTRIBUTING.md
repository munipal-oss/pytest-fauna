# Table of contents

[[_TOC_]]

# Install python dependencies

This project uses [poetry] for dependency and environment management. To get started,
you will need to follow the instructions for your system to [install poetry].

This project supports Python 3.8+. You might consider installing [pyenv] if your system
does not ship with one of those versions.

Once poetry is installed, change to the root of the repository and install all
dependencies including development and extra ones:

```bash
poetry install --all-extras
```

To install additional packages, add them via poetry:

```bash
poetry add <package name>
```

To install additional development packages (ones not needed for the lambda itself):

```bash
poetry add --group dev <package name>
```

# Commit format

This repository follows the [conventional commits] specification for commit messages.
This is to enable usage of [semantic release] to automatically generate releases,
changelogs, release notes, etc.

# Git hooks

This repository uses [pre-commit] in order to manage a set of hooks that do things like:

* ensure code is linted and has no errors
* ensure consistent formatting and styling

After you have [installed the dependencies] you can set up the git hooks by running:

```bash
poetry run pre-commit install
```

Now when you commit, it will automatically run the repo hooks and reject the commit if
something is wrong. Most hooks will automatically fix the problems for you, so in most
cases you will simply need to check the changes they made, add them, and commit again.

[poetry]: https://python-poetry.org/docs/
[install poetry]: https://python-poetry.org/docs/#installation
[pyenv]: https://github.com/pyenv/pyenv
[conventional commits]: https://www.conventionalcommits.org/en/v1.0.0/
[semantic release]: https://semantic-release.gitbook.io/semantic-release/
[pre-commit]: https://pre-commit.com/
[installed the dependencies]: #install-python-dependencies
