## [6.0.3](https://gitlab.com/munipal-oss/pytest-fauna/compare/v6.0.2...v6.0.3) (2025-01-03)

### Bug Fixes

* bump the `maximum_retries` value to 10 ([d31e3da](https://gitlab.com/munipal-oss/pytest-fauna/commit/d31e3da329edda82335fd2d26710591afadceac3))

## [6.0.2](https://gitlab.com/munipal-oss/pytest-fauna/compare/v6.0.1...v6.0.2) (2024-10-17)

### Bug Fixes

* add missing registration of FSL_DIR INI option ([e43af57](https://gitlab.com/munipal-oss/pytest-fauna/commit/e43af5796ac21207e9690389ecd94c0560513ffd))

## [6.0.1](https://gitlab.com/munipal-oss/pytest-fauna/compare/v6.0.0...v6.0.1) (2024-10-17)

### Bug Fixes

* **deps:** add missing pytest-testinfra dependency ([bc5dddf](https://gitlab.com/munipal-oss/pytest-fauna/commit/bc5dddf7a84d0063085dfe8f08a2fb099be56e9b))

## [6.0.0](https://gitlab.com/munipal-oss/pytest-fauna/compare/v5.0.1...v6.0.0) (2024-10-17)

### ⚠ BREAKING CHANGES

* `test_db_scoped_key` no longer returns a function for
creating scoped keys and instead returns a secret key scoped to the test
database. The fixture that returns a function for generating scoped keys
is now `build_scoped_key`.

### Features

* add support for applying FSL to the test DB ([b3948cc](https://gitlab.com/munipal-oss/pytest-fauna/commit/b3948cc50a2c543d3b94cac2b0c2fa5d04f5c788))

## [5.0.1](https://gitlab.com/munipal-oss/pytest-fauna/compare/v5.0.0...v5.0.1) (2024-10-10)

### Bug Fixes

* **deps:** update optional fluctuate dependency to require >= v4 ([cdd3c40](https://gitlab.com/munipal-oss/pytest-fauna/commit/cdd3c401470537fa3876d64dd7e01ebed94c4131))

## [5.0.0](https://gitlab.com/munipal-oss/pytest-fauna/compare/v4.0.0...v5.0.0) (2024-10-08)

### ⚠ BREAKING CHANGES

* **deps:** drop support for pytest-aws-fixtures ^2.0.0

### Bug Fixes

* **deps:** update dependency pytest-aws-fixtures to v3 ([860f05f](https://gitlab.com/munipal-oss/pytest-fauna/commit/860f05f90831a61de5ecc7a4bfa079a1ffc97015))

## [4.0.0](https://gitlab.com/munipal-oss/pytest-fauna/compare/v3.0.0...v4.0.0) (2024-10-07)

### ⚠ BREAKING CHANGES

* **deps:** drop support for pre-commit ^3.0.0

### Miscellaneous Chores

* **deps:** update dependency pre-commit to v4 ([5ba118c](https://gitlab.com/munipal-oss/pytest-fauna/commit/5ba118c035be2f35119eebba3f705040154e86f5))

## [3.0.0](https://gitlab.com/munipal-oss/pytest-fauna/compare/v2.0.0...v3.0.0) (2024-07-31)

### ⚠ BREAKING CHANGES

* **deps:** drop support for pytest-aws-fixtures ^1.2.0

### Bug Fixes

* **deps:** update dependency pytest-aws-fixtures to v2 ([d1e6ce1](https://gitlab.com/munipal-oss/pytest-fauna/commit/d1e6ce140a95c5158b85aeb9758befe9865ef392))

## [2.0.0](https://gitlab.com/munipal-oss/pytest-fauna/compare/v1.0.0...v2.0.0) (2024-05-30)

### ⚠ BREAKING CHANGES

* **deps:** this drops support for fauna ^1.0.0 and fluctuate ^2.0.0

### Bug Fixes

* **deps:** update fauna to v2 and fluctuate to v3 ([437d10e](https://gitlab.com/munipal-oss/pytest-fauna/commit/437d10e48da3f113098a8095b84993cdf5dde160))

## 1.0.0 (2024-04-22)


### Features

* initial release ([0835860](https://gitlab.com/munipal-oss/pytest-fauna/commit/08358609ea94acec82e0ef945d440347ca6fec5e))
